.. _intro:

Introduction
------------

ADE creates a Docker container from a base image and mounts additional read-only volumes in ``/opt``.
The base image provides development tools (e.g. vim, udpreplay, etc.), and the volumes provide
additional development tools (e.g., IDEs, large third-party libraries) or released software versions
(e.g. the latest AutowareAuto release). Furthermore, ADE enables easy switching between versions of
the images and volumes.

An example of ADE
^^^^^^^^^^^^^^^^^

.. image:: ADE.png

The image above shows an example of an ADE configuration. The base-image, ``ade``, is used to create
a container where volumes for *AutowareAuto* (a software library), *Atom* (an IDE), and *CUDA* (a large third-party
library) are mounted as volumes.

When ADE is started, it prints out a corresponding matrix: e.g.

::

   ade               | v0.1.0   | v0-1-0  | registry.gitlab.com/autowareauto/autowareauto/ade:v0-1-0
   autowareauto      | v0.1.0   | v0-1-0  | registry.gitlab.com/autowareauto/autowareauto:v0-1-0
   atom              | v1.35.0  | latest  | registry.gitlab.com/apexai/ade-atom:latest
   cuda              | v8.0     | v8-0    | registry.private-gitlab/cuda/cuda:latest

The first row of the table is the *base image*, all other entries are *volumes* and have a corresponding
entry in ``/opt``. Each row displays the image name, version information (git hash or tag), the Docker tag,
and the FQN for each image. If no version information was provided during the creation of the Docker image,
the second column will be empty.


Terminology
^^^^^^^^^^^

Docker
   Docker allows the creation of containers with isolated resources from the host OS:
   “Docker is a computer program that performs operating-system-level virtualization also
   known as containerization.” — `Wikipedia Docker (software)`_.

Image
   Images are snapshots of the filesystem for a container. They are based on a ``Dockerfile``, and
   are created using ``docker build``. Images can be uploaded to a *registry*, where they can be downloaded
   by others.

Container
   A container is a running instance of an image. When a container is deleted, any changes to its
   filesystem are usually discarded. A container can publish parts of its filesystem as *volumes*,
   which can then be mounted by other containers.

Registry
   A registry is a server-side application that stores pre-built Docker images.

Fully-qualified name (FQN)
   The FQN is the term for the full URL-like path that describes an image.
   e.g. ``registry.gitlab.com/apexai/ade-atom:latest``

Tag
   A tag is a specific version of an image. e.g. ``latest`` in the FQN above

Host
   The host is the native operating system on which Docker is running.


.. _Wikipedia Docker (software): https://en.wikipedia.org/wiki/Docker_%28software%29
