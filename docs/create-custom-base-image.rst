.. _create-custom-base-image:

Creating a custom base image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :hidden:

   entrypoint-file


Writing the Dockerfile
""""""""""""""""""""""

The `Dockerfile`_ in the `minimal-ade project`_ shows a minimal example of how to create
a custom base image:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker

Let's walk through it:

1. An existing Docker image

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 1

2. Some convenience programs and requirements of the ``entrypoint``:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 3-17


3. env.sh_:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 19

   * Configures the default environment on ``ade enter``
   * Source the ``.env.sh`` files provided by volumes (see :ref:`create-custom-volume`)

4. entrypoint_ :

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 20-21

   See also :ref:`entrypoint-file`.

5. ``CMD``:

   .. literalinclude:: minimal-ade/Dockerfile
      :language: docker
      :lines: 22

   * This command attaches a process to PID 1 which will keep the container running even if no one
     is attached to the container


Building the image
""""""""""""""""""

Once the ``Dockerfile`` is ready, build the image by running ``docker build`` in the directory
that contains the ``Dockerfile``::

   docker build -t <image_name>:<tag> .

Add ``--label ade_image_commit_sha=<git-hash>`` and/or ``--label ade_image_commit_tag=<git-tag>`` to
embed VCS information in the Docker image::

   docker build \
     --label ade_image_commit_sha=<git-hash> \
     --label ade_image_commit_tag=<git-tag> \
     -t <image_name>:<tag> .

For more information, see the `docker build`_ documentation


Using the image
"""""""""""""""

To use the image, add it to the ``.aderc`` file as the first
entry in ``ADE_IMAGES``::

   export ADE_IMAGES="
      <image_name>:<tag>
      ...
   "

For more information, see :ref:`aderc-file`


.. _Dockerfile: https://gitlab.com/ApexAI/minimal-ade/blob/master/Dockerfile
.. _minimal-ade project: https://gitlab.com/ApexAI/minimal-ade
.. _env.sh: https://gitlab.com/ApexAI/minimal-ade/blob/master/env.sh
.. _entrypoint: https://gitlab.com/ApexAI/minimal-ade/blob/master/entrypoint
.. _docker build: https://docs.docker.com/engine/reference/commandline/build/
